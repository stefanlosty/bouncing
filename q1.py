# -*- coding: utf-8 -*-
"""
Created on Mon Aug  5 17:46:33 2019

@author: SurfacePro3
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy

def odef(x,t1):
    dxdt = x
    return dxdt

def euler_step(f, x1, t1, t2):
    dxdt = f(x1, t1)
    dt = t2-t1
    x2 = x1 + dt *dxdt
    return x2

def solve_to(x1,t1,t2):
    x2 = euler_step(odef,x1,t1,t2)
    return x2

def solve_ode(t1,t2):
    timesteps = int((t2-t1)/delta_t)
    solution = np.zeros(timesteps)
    solution[0] = x0
    for i in range(timesteps-1):
        solution[i+1] = solve_to(solution[i],t1,t1 + delta_t)
        t1 += delta_t
    return solution

x0 = 1
delta_t = 0.01

solution = solve_ode(0, 5)

#plot
x = np.linspace(0,5,500)
y = solution
y1 = x**np.e

fig = plt.figure(figsize = (8,6))
ax = fig.add_subplot(1,1,1)
ax.plot(x,y, color = 'black', linewidth = 3)
ax.plot(x,y1, color = 'red', linewidth = 3)
ax.set_xlim([0, 5])
plt.yscale("log")
ax.set_ylim([0, solution.max()])

ax.set_xlabel(r'$t$', fontsize = 'large')
ax.set_ylabel(r'$x$', rotation=0)

plt.show()