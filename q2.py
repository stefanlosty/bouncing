# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 14:23:59 2019

@author: SurfacePro3
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy

def odef(x,t):
    dxdt = x
    return dxdt

def rk4_step(f, x1, t1):
    k1 = h*f(x1, t1)
    k2 = h*f(x1+k1/2, t1+h/2)
    k3 = h*f(x1+k2/2, t1+h/2)
    k4 = h*f(x1+k3, t1+h)
    x2 = x1 + 1/6*(k1+2*k2+2*k3+k4)
    return x2

def euler_step(f, x1, t1):
    dxdt = f(x1, t1)
    x2 = x1 + h*dxdt
    return x2

def solve_to(x1,t1,method):
    if method == "rk4":
        x2 = rk4_step(odef,x1,t1)
    elif method == "euler":
        x2 = euler_step(odef,x1,t1)
    return x2

def solve_ode(t1,t2,method):
    solution = np.zeros(timesteps)
    solution[0] = x0
    for i in range(timesteps-1):
        solution[i+1] = solve_to(solution[i],t1,method)
        t1 += h
    return solution

x0 = 1
h = 0.002
t1 = 0
t2 = 10
timesteps = int((t2-t1)/h)

solution = solve_ode(t1, t2, "rk4")
solution2 = solve_ode(t1, t2, "euler")

#plot
x = np.linspace(t1,t2,timesteps)
y = solution
z = solution2
y1 = x**np.e

fig = plt.figure(figsize = (8,6))
ax = fig.add_subplot(1,1,1)
ax.plot(x,y, color = 'black', linewidth = 3)
ax.plot(x,y1, color = 'red', linewidth = 3)
ax.plot(x,z, color = 'g', linewidth = 3)
ax.set_xlim([0, t2])
#plt.yscale("log")
ax.set_ylim([0, solution.max()])

ax.set_xlabel(r'$t$', fontsize = 'large')
ax.set_ylabel(r'$x$', rotation=0)

plt.show()